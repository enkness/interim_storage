%{
    #include <stdio.h>

%}
%%
P: S { printf ( "length: %d\n", $1 ); }
S: '('S')'S { $$ = 1 + $4;}
| /*empty*/{ $$ = 0; } %%
main () {
    printf ( "type a string, please: " ); yyparse ();
}
yylex () {
    int c;
    c = getchar ();

    if ( c=='\n' ) return 0; yylval = c;

    return c;
}

int yyerror ( char *s ) {
    printf ( "Depth eval: %s\n", s );
}