%{
    #include<stdio.h>
    int flag=0;
%}

%token NUMBER
%left '+' '-'
%left '*' '/'
%left '(' ')'

%%
   ArithmeticExpression: E {
      printf("\nResult = %d\n",$$);
      return 0;
   };

   E: 
      E'+'E {$$=$1+$3;}
      |E'-'E {$$=$1-$3;}
      |E'*'E {$$=$1*$3;}
      |E'/'E {if($3 == 0) { yyerror(); return 1;} $$=$1/$3;}
      |'('E')' {$$=$2;}
      | NUMBER {$$=$1;}
      | '-'NUMBER {$$ = -$2;}
      | '+'NUMBER {$$ = $2;}
   ;
%%

void main() {
   printf("\nEnter Expression : ");

   yyparse();

   if(flag==0) printf("\nSuccess\n\n");
}

int yyerror(const char* str) {
   printf("\nError\n\n");
   flag=1;
}