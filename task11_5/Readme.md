# Task11_5

## Tasks

| № | Task                                     | Solution |
|:-:|:-----------------------------------------|:-------- |
| 1 |Calculate the length of the bracket system| `length` |
| 2 |Build a calculator program for expressions containing integers, operations `+`, `*` , `−` , `/` and `(` `)`|`calc`  |


## Compile
`make`

## Run
Run length : `./length`

Run calc : `./calc`